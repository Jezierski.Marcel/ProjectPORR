#include <stdio.h>
#include <stdlib.h>
#include "string.h"
#include <time.h>


#define LINEARSIZE 1000


//logging codes
#define INFO 0
#define ERROR -1

int logging(int status, int threadNum,char description[]){
    switch(status){
        case INFO:
            printf("--INFO--  Thread--> %i -- Description-->   %s\n", threadNum, description);
            break;
        case ERROR:
            printf("--ERROR-- Thread--> %i -- Description-->   %d\n", threadNum, description);
            break;

    }
}
void initiateArrayWithRandomNumbers(int * array){
    int temp[LINEARSIZE];
    for(int i=0; i < LINEARSIZE; i++){
        temp[i] = i;
    }
    for(int i=0; i < LINEARSIZE; i++){
        int r = i + rand() % (LINEARSIZE - i);
        array[i] = temp[r];
        temp[r] = temp[i];
    }
}
void printArrayElements(int * array){
    printf("[ ");
    for(int i=0; i<LINEARSIZE; i++){
        printf(" %i ", array[i]);
    }
    printf(" ]");
}

int main() {
    //kohan branch here

    int x[LINEARSIZE] = {0};
    logging(INFO, 1, "abcds");
    initiateArrayWithRandomNumbers(x);
    printArrayElements(x);
    return 0;
}